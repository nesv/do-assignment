# Digital Ocean interview assignment

This repository contains an implementation of the Digital Ocean assignment
handed out for interviewing candidates.

## Building

The following tools are required to build this project:

- Go >=1.14
- GNU make

To build this assignment, there is a GNUmakefile included in the respository.
Run

	make

to build an executable binary in the current directory.

### Docker

This repository also comes with a Dockerfile.
You will need to have [Docker](https://www.docker.com) installed to be able
to build a Docker image.
There is a makefile target provided for convenience:

	make docker-image

## Tests

To run all tests included in this repository, run

	make check

If you wish to only test a subset of packages within this repository,
you may set the `pkg` macro when invoking the `check` target.
For example, to test only the `pkg/index` package:

	make check pkg=pkg/index

## Design rationale

### The `Store` interface

The `Store` interface is defined in `pkg/store/store.go`.
Within the same package, an ephemeral, in-memory implementation is provided.

The decision behind making this type an interface, was to allow for future
implementation that can use any persistent datastore (e.g. Redis, PostgreSQL).
I would have liked to provide a second implementation using something like
Redis, but the project constraints stipulated to not use any libraries beyond
those supplied by the chosen language's standard library.

### Circular dependency detection

The in-memory implementation of the `Store` interface checks to ensure that
circular dependencies are not introduced.
The check is performed both when a package is being initially submitted to the
index, and when a package is re-submitted to the index (likely to update its
dependencies).
When a circular dependency is detected during an INDEX request, the server
will return a FAIL response, and emit a log message.

While circular dependencies could be remedied by re-indexing a package, and
modifying its dependency list to remove the cycle, I figured it would be
prudent to prevent circular dependencies from being introduced in
the first place.

### Request parsing and validation

The `pkg/proto.{Request,Response}` types do not implement any sort of data
validation when &mdash; for example &mdash; parsing a request off the wire.
In an initial implementation the `Request` type did attempt to validate the
command, and package name as per the assignment's instructions, but the
validation was then semi-duplicated in the `pkg/proto.Server.handle` function.

In this implementation, to keep validation logic in one place, the `Request`
type's `UnmarshalText` method only validates the shape of the data; it only
checks to make sure there are three, pipe-separated fields before the newline
character.
Any further validation of the request &mdash; checking the command, and making
sure the mandatory package name was provided &mdash; is handled by the
`Server`.

## Rust

To keep up with learning Rust, I also implemented a Rust version of the
assignment.
It works, but I have not taken the time to go through and optimize anything,
and it runs a little slower than the Go version.

The Rust version was built and tested on Rust 1.43.1.

To build the Rust version, you should have the Rust toolchain installed.
Run:

	cargo build --release

To run the built version:

	./target/release/do-assignment

### Speed comparison

I really need to profile the Rust version, as on some initial tests, it is
a fair bit slower than the Go version.

Initially, a release build of the assignment was coming in at ~4-5s per run.
A noticeable improvement was made by giving each client thread its own,
buffered writer, to STDERR.
This way, there would be fewer syscalls made over the lifetime of a test run.

### Threading

Unlike Go, Rust does not have any analog to goroutines in its standard library.
There are external crates like [tokio](https://tokio.rs/) and
[async-std](https://docs.rs/async-std), however they are both external to
Rust's standard library, and using them would be in violation of the
assignment's instructions.

Alas, shy of implementing my own asynchronous runtime, I opted to use
[`std::thread`](https://doc.rust-lang.org/std/thread/), and handle each client connection on its own thread.
Using one thread per connection is fine for this example, but in reality,
if there were enough clients, there is a very good chance we could run the risk
of thread starvation.
