package proto

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"

	"assignment/pkg/store"
)

// NewServer instantiates a new server, and can configure it with any
// caller-provided options.
//
// By default, if no options are provided, the server will log to
// ioutil.Discard, and use an in-memory data store for holding the package
// index.
func NewServer(options ...ServerOption) (*Server, error) {
	srv := Server{
		store: store.NewMemoryStore(),
		log:   log.New(ioutil.Discard, "", log.LstdFlags),
	}

	for i, opt := range options {
		if err := opt(&srv); err != nil {
			return nil, fmt.Errorf("apply option %d: %w", i, err)
		}
	}

	return &srv, nil
}

// Server is an implementation of a network server that handles requests for
// indexing packages, removing packages from an index, and querying the package
// index.
type Server struct {
	store store.Store
	log   *log.Logger
}

// Serve accepts and handles connections from lst, and will close lst when
// it returns.
// Serve will return when ctx is cancelled.
func (s *Server) Serve(ctx context.Context, lst net.Listener) error {
	defer lst.Close()

	// Start a goroutine for accepting connections.
	// Any accepted connection will be sent on "conns".
	conns := make(chan net.Conn)
	go func(conns chan<- net.Conn) {
		defer close(conns)
		for {
			conn, err := lst.Accept()
			if err != nil {
				s.log.Printf("Accept connection: %v", err)
				select {
				case <-ctx.Done():
					return
				default:
					continue
				}
			}

			select {
			case conns <- conn:
			case <-ctx.Done():
				return
			}
		}
	}(conns)

	// Loop until our context is done.
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()

		case conn, ok := <-conns:
			if !ok {
				// The channel has been closed.
				return nil
			}
			go s.handle(ctx, conn)
		}
	}
}

func (s *Server) handle(ctx context.Context, conn net.Conn) {
	defer conn.Close()
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		var req Request
		if _, err := req.ReadFrom(conn); err == io.EOF {
			return
		} else if err != nil {
			s.log.Printf("Read message from %s: %v", conn.RemoteAddr(), err)
			fmt.Fprintln(conn, "ERROR")
			continue
		}

		// The Request.Pacakge field is required.
		if req.Package == "" {
			fmt.Fprintln(conn, "ERROR")
			continue
		}

		// Decide what to do based on the command in the request.
		var resp Response = OK
		switch req.Command {
		case CommandIndex:
			if err := s.store.Index(req.Package, req.Dependencies...); err != nil {
				s.log.Printf("%s %q: %v", req.Command, req.Package, err)
				resp = Fail
			}

		case CommandRemove:
			if err := s.store.Remove(req.Package); err != nil {
				s.log.Printf("%s %q: %v", req.Command, req.Package, err)
				resp = Fail
			}

		case CommandQuery:
			if err := s.store.Query(req.Package); err != nil {
				s.log.Printf("%s %q: %v", req.Command, req.Package, err)
				resp = Fail
			}

		default:
			// Some unknown command.
			resp = Error
		}

		if _, err := resp.WriteTo(conn); err != nil {
			return
		}
	}
}
