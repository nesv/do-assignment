package strutil

// SliceIndex returns the index of the first instance of s in strs, or -1 if
// s is not present in strs.
func SliceIndex(strs []string, s string) int {
	n := -1
	for i, v := range strs {
		if v == s {
			n = i
			break
		}
	}
	return n
}
