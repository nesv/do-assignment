package proto

import (
	"bufio"
	"io"
)

// Response represents the newline-terminated response from a Request.
type Response string

const (
	// OK is a response returned to a client when the request has
	// succeeded.
	OK = Response("OK")

	// Fail is the response returned to a client when the request
	// cannot be fulfilled.
	Fail = Response("FAIL")

	// Error is the response returned to a client when the request
	// is malformed.
	Error = Response("ERROR")

	emptyResponse = Response("")
)

// String is provided to implement the fmt.Stringer interface for easy printing
// of a response.
func (r Response) String() string {
	return string(r)
}

// ReadFrom consumes bytes from rdr until a "\n" character is reached.
// The newline character is consumed as well.
func (r *Response) ReadFrom(rdr io.Reader) (int64, error) {
	scanner := bufio.NewScanner(rdr)
	if !scanner.Scan() {
		return 0, scanner.Err()
	}

	*r = Response(scanner.Text())
	return int64(len(scanner.Text())), nil
}

// WriteTo writes r to w, returning the number of bytes written to w, and
// possibly an error.
func (r Response) WriteTo(w io.Writer) (int64, error) {
	n, err := w.Write(append([]byte(r), '\n'))
	return int64(n), err
}
