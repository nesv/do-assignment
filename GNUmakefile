NAME	:= assignment
SOURCES	:= $(wildcard *.go) \
	$(wildcard pkg/proto/*.go) \
	$(wildcard pkg/store/*.go) \
	$(wildcard pkg/strutil/*.go)
VERSION	?= $(shell date +%Y%m%d-%s)

DOCKER	:= $(shell which docker 2>/dev/null)
GO	:= $(shell which go 2>/dev/null)
GO_TESTFLAGS	:= -cover -race
GO_BUILDTAGS	?=
GO_BUILD	:= ${GO} build -tags="${GO_BUILDTAGS}"

# The location to install the final binary into.
DESTDIR	?= /usr/local/bin

all: ${NAME}

${NAME}: ${SOURCES}
ifeq (${GO},)
	$(error Cannot find "go" in your $$PATH)
endif
	${GO_BUILD} -o $@ ./

.PHONY: install
install: ${NAME}
	install -m 0755 -t ${DESTDIR} $^

.PHONY: clean
clean:
	-rm -f ${NAME}

.PHONY: check
check:
ifeq (${GO},)
	$(error Cannot find "go" in your $$PATH)
endif
ifeq (${pkg},)
	${GO} test ${GO_TESTFLAGS} ./...
else
	${GO} test ${GO_TESTFLAGS} ./${pkg}
endif

.PHONY: bench
bench:
ifeq (${GO},)
	$(error Cannot find "go" in your $$PATH)
endif
ifeq (${pkg},)
	${GO} test ${GO_TESTFLAGS} -bench=. -benchmem ./...
else
	${GO} test ${GO_TESTFLAGS} -bench=. -benchmem ./${pkg}
endif

.PHONY: docker-image
docker-image: Dockerfile
ifeq (${DOCKER},)
	$(error Cannot find "docker" in your $$PATH)
endif
	${DOCKER} build -t assignment:${VERSION} -f $< --build-arg VERSION=${VERSION} .
	${DOCKER} tag assignment:${VERSION} assignment:latest
