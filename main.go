package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"assignment/pkg/proto"
	"assignment/pkg/store"
)

var (
	// Set by command-line flags.
	listenAddr string
	logEnabled bool
	storeDSN   string
	pprofAddr  string
)

func main() {
	flag.StringVar(&listenAddr, "listen", "127.0.0.1:8080", "`addr:port` to listen on")
	flag.StringVar(&storeDSN, "store", "memory://", "URL indicating the datastore to use for storing the package index")
	flag.BoolVar(&logEnabled, "log", false, "Enable logging")
	flag.StringVar(&pprofAddr, "pprof", "", "Enable the pprof HTTP listener at the specified `host:port`")
	flag.Parse()

	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	if pprofAddr != "" {
		log.Println("Starting pprof listener at", pprofAddr)
		go http.ListenAndServe(pprofAddr, nil)
	}

	// Set up our server, starting with our options.
	var srvopts []proto.ServerOption
	if logEnabled {
		srvopts = append(srvopts, proto.WithLogger(log.New(os.Stderr, "", log.LstdFlags)))
	}

	ds, err := store.New(storeDSN)
	if err != nil {
		return fmt.Errorf("new store: %w", err)
	}
	srvopts = append(srvopts, proto.WithStore(ds))

	srv, err := proto.NewServer(srvopts...)
	if err != nil {
		return fmt.Errorf("new server: %w", err)
	}

	// Set up a new TCP listener.
	lst, err := net.Listen("tcp", listenAddr)
	if err != nil {
		return fmt.Errorf("start listener: %w", err)
	}

	// Set up a new wait group for keeping track of how many of our
	// goroutines are running.
	var wg sync.WaitGroup
	wg.Add(1)

	// When all of the goroutines exit, they will close the "done" channel,
	// which will be one of the cases that will break us out of our
	// mainloop, below.
	done := make(chan bool)
	go func() {
		wg.Wait()
		close(done)
	}()

	// Start a new context that can be used for telling any of our goroutines
	// that it is time to shut down.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Start the server in its own goroutine.
	srverrs := make(chan error)
	go func(ctx context.Context, srv *proto.Server, errs chan<- error) {
		defer wg.Done()
		defer close(srverrs)
		log.Printf("Starting listener on %s", lst.Addr())
		if err := srv.Serve(ctx, lst); err != nil && err != context.Canceled {
			errs <- err
		}
	}(ctx, srv, srverrs)

	// Listen for signals from the OS, or from any error from the server
	// goroutine.
	signals := make(chan os.Signal)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)
	for {
		select {
		case s := <-signals:
			log.Printf("Received %s: shutting down", s)
			cancel()
			<-done
			return nil

		case <-done:
			// All of our goroutines have died.
			log.Println("All goroutines are dead")
			return errors.New("all goroutines are dead")

		case err := <-srverrs:
			cancel()
			<-done
			return fmt.Errorf("serve: %w", err)
		}
	}
	return nil
}
