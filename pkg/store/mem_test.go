package store

import (
	"testing"

	"assignment/pkg/strutil"
)

func TestNewMemoryStore(t *testing.T) {
	t.Run("Index", func(t *testing.T) {
		t.Parallel()
		idx := NewMemoryStore()
		tests := []struct {
			name string
			deps []string
			fail bool
		}{
			{
				name: "ceylon",
			},

			// This test should fail, since none of the dependencies
			// are indexed.
			{
				name: "cloog",
				deps: []string{"gmp", "isl", "pkg-config"},
				fail: true,
			},

			// These next few tests will populate the index with
			// cloog's dependencies, then make another attempt to
			// index cloog (which should pass).
			{
				name: "gmp",
			},
			{
				name: "isl",
			},
			{
				name: "pkg-config",
			},
			{
				name: "cloog",
				deps: []string{"gmp", "isl", "pkg-config"},
			},
		}
		for _, tt := range tests {
			err := idx.Index(tt.name, tt.deps...)
			if err == nil && tt.fail {
				t.Error("expected non-nil error")
			} else if err != nil && tt.fail {
				// This is totally okay.
				// We are expecting this test to fail.
			} else if err != nil {
				t.Errorf("index %s: %v", tt.name, err)
			}
		}

		// Verify the short-circuiting when an zero-length string
		// is passed as the package name.
		t.Run("nopkgname", func(t *testing.T) {
			if err := idx.Index(""); err == nil {
				t.Error("expected non-nil error")
			}
		})

		// Add a package with dependencies, then re-add the package
		// with a different set of dependencies, and verify the
		// dependencies, along with all reverse dependencies, have been
		// updated accordingly.
		t.Run("UpdateRemoveDep", func(t *testing.T) {
			t.Parallel()
			index := NewMemoryStore()
			idx, ok := index.(*inmem)
			if !ok {
				t.Fatal("cannot cast return of NewMemoryStore to *inmem")
			}

			for _, name := range []string{"a", "b"} {
				if err := idx.Index(name); err != nil {
					t.Fatalf("index %q: %v", name, err)
				}
			}

			if err := idx.Index("c", "a", "b"); err != nil {
				t.Fatal("index c:", err)
			}

			// Re-index "c", removing "b" as a dependency.
			if err := idx.Index("c", "a"); err != nil {
				t.Fatal("re-index c:", err)
			}

			// Check to make sure "c" isn't in the list of
			// reverse dependencies for "b".
			rdeps, ok := idx.rev["b"]
			if !ok {
				t.Fatal("expected reverse mapping for package b")
			}
			for _, d := range rdeps {
				if d == "c" {
					t.Fatal("found package c in reverse-deps for b")
				}
			}

			// Verify the dependency list for "c" has been updated.
			deps, ok := idx.pkgs["c"]
			if !ok {
				t.Fatal("nil dependency list for c")
			}
			for _, d := range deps {
				if d == "b" {
					t.Fatal("found package b in deps for c")
				}
			}
		})

		t.Run("UpdateAddDep", func(t *testing.T) {
			t.Parallel()
			index := NewMemoryStore()
			idx, ok := index.(*inmem)
			if !ok {
				t.Fatal("cannot cast return of NewMemoryStore to *inmem")
			}

			for _, name := range []string{"a", "b"} {
				if err := idx.Index(name); err != nil {
					t.Fatalf("index %q: %v", name, err)
				}
			}

			// Index a package with one dependency.
			if err := idx.Index("c", "a"); err != nil {
				t.Fatal("index c:", err)
			}

			// Ensure the dependency list for "c" only includes "a",
			// and that the reverse-dependencies for "a" only includes
			// "c".
			cdeps, ok := idx.pkgs["c"]
			if !ok {
				t.Fatal("nil dependency list for c")
			}
			if len(cdeps) != 1 {
				t.Fatalf("expected 1 dependency for c, found %d", len(cdeps))
			}
			if cdeps[0] != "a" {
				t.Fatalf("c deps[0]: want=%q got=%q", "a", cdeps[0])
			}

			ardeps, ok := idx.rev["a"]
			if !ok {
				t.Fatal("nil reverse-dependency list for a")
			}
			if len(ardeps) != 1 {
				t.Fatalf("expected 1 reverse dependency for a, found %d", len(cdeps))
			}
			if ardeps[0] != "c" {
				t.Fatalf("a rdeps[0]: want=%q got=%q", "c", ardeps[0])
			}

			// Re-index the package, adding "b" as a dependency.
			if err := idx.Index("c", "a", "b"); err != nil {
				t.Fatal(err)
			}

			// Verify c's dependency list was updated.
			cdeps, ok = idx.pkgs["c"]
			if !ok {
				t.Fatal("nil dependency list for c")
			}
			if strutil.SliceIndex(cdeps, "b") == -1 {
				t.Error("b not added to c's dependencies")
			}

			// Verify b's reverse dependencies were updated.
			brdeps, ok := idx.rev["b"]
			if !ok {
				t.Fatal("nil reverse dependency list for b")
			}
			if strutil.SliceIndex(brdeps, "c") == -1 {
				t.Errorf("c not added to b's reverse dependencies")
			}
		})

		t.Run("CircularDependencies", func(t *testing.T) {
			t.Parallel()
			idx := NewMemoryStore()

			if err := idx.Index("a"); err != nil {
				t.Fatal("index a:", err)
			}

			if err := idx.Index("b", "a"); err != nil {
				t.Fatal("add a as a dependency of b:", err)
			}

			// Introduce the circular dependency.
			// This should fail.
			if err := idx.Index("a", "b"); err == nil {
				t.Error("circular dependency allowed")
			}
		})

		// Index, and re-index a package, making no changes to the
		// dependencies.
		t.Run("UpdateNoop", func(t *testing.T) {
			t.Parallel()
			idx := NewMemoryStore()
			for _, name := range []string{"a", "b"} {
				if err := idx.Index(name); err != nil {
					t.Fatalf("index %q: %v", name, err)
				}
			}

			if err := idx.Index("c", "a", "b"); err != nil {
				t.Fatal(err)
			}

			// Re-index the same package, with the same
			// dependencies, but shuffle them to make things a
			// little spicy.
			if err := idx.Index("c", "b", "a"); err != nil {
				t.Fatal(err)
			}
		})
	})

	t.Run("Remove", func(t *testing.T) {
		t.Parallel()
		idx := NewMemoryStore()

		// Add a package without any dependencies, and remove it.
		t.Run("basic", func(t *testing.T) {
			if err := idx.Index("a"); err != nil {
				t.Fatal(err)
			}
			if err := idx.Remove("a"); err != nil {
				t.Fatal(err)
			}
		})

		// Add some packages, including one with dependencies, and try
		// to remove one of the dependant pacakges.
		// This should return a non-nil error.
		t.Run("dependants", func(t *testing.T) {
			if err := idx.Index("a"); err != nil {
				t.Fatal(err)
			}
			if err := idx.Index("b", "a"); err != nil {
				t.Fatal(err)
			}

			// Try and remove "a".
			// This should fail, as it is a dependant of "b".
			if err := idx.Remove("a"); err == nil {
				t.Fatal("remove a: expected non-nil error")
			}

			// Now, remove "b", then "a".
			// This should pass.
			if err := idx.Remove("b"); err != nil {
				t.Error("remove b:", err)
			}
			if err := idx.Remove("a"); err != nil {
				t.Error("remove a:", err)
			}
		})

		// Remove a package that is not in the index.
		t.Run("unindexed", func(t *testing.T) {
			if err := idx.Remove("foo"); err != nil {
				t.Errorf("expected nil error, got=%q", err)
			}
		})

		// Verify the short-circuiting if the package name is empty.
		t.Run("nopkgname", func(t *testing.T) {
			if err := idx.Remove(""); err != nil {
				t.Errorf("expected nil error, got=%q", err)
			}
		})
	})

	t.Run("Query", func(t *testing.T) {
		t.Parallel()
		idx := NewMemoryStore()

		if err := idx.Index("a"); err != nil {
			t.Fatalf("expected non-nil error, got=%q", err)
		}

		t.Run("nopkgname", func(t *testing.T) {
			if err := idx.Query(""); err != nil {
				t.Errorf("expected nil error, got=%q", err)
			}
		})

		t.Run("unindexed", func(t *testing.T) {
			if err := idx.Query("b"); err != ErrNoExist {
				t.Errorf("want=%q got=%q", ErrNoExist, err)
			}
		})

		t.Run("default", func(t *testing.T) {
			if err := idx.Query("a"); err != nil {
				t.Errorf("expected nil error, got=%q", err)
			}
		})
	})
}
