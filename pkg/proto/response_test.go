package proto

import (
	"bytes"
	"strings"
	"testing"
)

func TestResponse(t *testing.T) {
	t.Run("ReadFrom", func(t *testing.T) {
		for _, txt := range []string{"OK\n", "FAIL\n", "ERROR\n", "\n", "noop\n"} {
			var resp Response
			if _, err := resp.ReadFrom(strings.NewReader(txt)); err != nil {
				t.Errorf("%s: %v", txt, err)
			}
		}
	})

	t.Run("WriteTo", func(t *testing.T) {
		for _, txt := range []string{"OK", "FAIL", "ERROR", "", "noop"} {
			var (
				buf  bytes.Buffer
				resp = Response(txt)
			)
			n, err := resp.WriteTo(&buf)
			if err != nil {
				t.Fatalf("%s: %v", txt, err)
			}
			if want, got := int64(len(txt)+1), n; want != got {
				t.Errorf("bytes written: want=%d got=%d", want, got)
			}
			if want, got := txt, resp.String(); want != got {
				t.Errorf("want=%q got=%q", want, got)
			}
		}
	})
}

func BenchmarkResponse(b *testing.B) {
	b.Run("ReadFrom", func(b *testing.B) {
		var (
			text = "ERROR\n"
			resp Response
		)
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			if _, err := resp.ReadFrom(strings.NewReader(text)); err != nil {
				b.Error(err)
			}
		}
	})

	b.Run("WriteTo", func(b *testing.B) {
		var buf bytes.Buffer
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			if _, err := Fail.WriteTo(&buf); err != nil {
				b.Error(err)
			}
			buf.Reset()
		}
	})
}
