// Package proto provides an implementation of the text-based protocol used for
// storing an index of software packages.
//
// This package includes a server implementation with a pluggable data store,
// as well as a client.
package proto
