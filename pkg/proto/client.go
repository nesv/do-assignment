package proto

import (
	"errors"
	"fmt"
	"net"
	"runtime"
	"sync"
	"sync/atomic"
)

// Dial establishes a TCP connection to the server at the given host and port.
func Dial(hostport string) (*Client, error) {
	// The maximum number of connections we are going to allow our
	// connection pool to have at any given time.
	limit := runtime.NumCPU()

	return &Client{
		addr:     hostport,
		conns:    make(chan net.Conn, limit),
		maxConns: uint32(limit),
	}, nil
}

// Client is a client for the line-oriented text protocol for indexing
// packages.
//
// Internally, the client uses a connection pool that allows runtime.NumCPU
// connections to be active at any given time.
// If there are no free connections in the pool, the client will establish
// a new connection, and shut it down afterwards.
type Client struct {
	addr        string
	maxConns    uint32
	activeConns uint32 // Only use with atomics.

	mu    sync.RWMutex
	conns chan net.Conn
}

func (c *Client) Index(pkgname string, dependencies ...string) (Response, error) {
	if pkgname == "" {
		return emptyResponse, errors.New("empty package name")
	}

	return c.Do(NewRequest(CommandIndex, pkgname, dependencies...))
}

func (c *Client) Remove(pkgname string) (Response, error) {
	if pkgname == "" {
		return emptyResponse, errors.New("empty package name")
	}

	return c.Do(NewRequest(CommandRemove, pkgname))
}

func (c *Client) Query(pkgname string) (Response, error) {
	if pkgname == "" {
		return emptyResponse, errors.New("empty package name")
	}

	return c.Do(NewRequest(CommandQuery, pkgname))
}

func (c *Client) Do(req Request) (Response, error) {
	var resp Response
	err := c.withConn(func(conn net.Conn) error {
		// Write the message out.
		_, err := req.WriteTo(conn)
		if err != nil {
			return fmt.Errorf("write message: %w", err)
		}

		if _, err := resp.ReadFrom(conn); err != nil {
			return fmt.Errorf("read response: %w", err)
		}

		return nil
	})
	return resp, err
}

// withConn gets a connection from the connection pool and invokes fn with the
// newly-acquired net.Conn.
// If there are no connections available in the pool, withConn will create a
// new one and close it out immediately.
func (c *Client) withConn(fn func(conn net.Conn) error) error {
	return c.withRLock(func() error {
		select {
		case conn := <-c.conns:
			err := fn(conn)
			c.conns <- conn
			return err

		default:
			conn, err := net.Dial("tcp", c.addr)
			if err != nil {
				return err
			}

			if err := fn(conn); err != nil {
				return err
			}

			if n := atomic.AddUint32(&c.activeConns, 1); n < c.maxConns {
				c.conns <- conn
			} else {
				conn.Close()
			}
		}
		return nil
	})
}

func (c *Client) withRLock(fn func() error) error {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return fn()
}

func (c *Client) withLock(fn func() error) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	return fn()
}

func (c *Client) Close() error {
	return c.withLock(func() error {
		if c.conns == nil {
			return nil
		}

		close(c.conns)

		for conn := range c.conns {
			conn.Close()
		}

		c.conns = nil
		return nil
	})
}
