// Package store provides the interface for a data store for a package
// index, along with an implementation that stores information in memory.
package store

import (
	"errors"
	"fmt"
	"io"
	"net/url"
)

var (
	// ErrNoExist is a sentinel error used to indicate that a package does
	// not exist in the index.
	ErrNoExist = errors.New("package does not exist")
)

// Store defines the interface for a type that is capable of storing an
// index of package names, and their dependencies.
type Store interface {
	io.Closer

	// Add a package name to the index, with zero or more dependencies.
	// If a dependency is not already in the index, Index will return a
	// non-nil error.
	//
	// When a package name is already indexed, Index must replace that
	// package's dependencies with those specified.
	//
	// Index must also handle reverse dependencies.
	// If package A depends on package B, storing the reverse dependency
	// from B -> A should be handled as well.
	Index(name string, dependencies ...string) error

	// Remove a package from the index.
	// If the package has dependants (meaning it is depended on by other
	// packages), Remove will return a non-nil error.
	//
	// If the package is not in the index, Remove will return a nil error.
	//
	// Remove may panic, depending on the implementation, to indicate that
	// assumptions about the rest of the implementation have not been met.
	// For example, if reverse dependencies are not properly handled within
	// Index, Remove should be written to verify that Index is handling
	// reverse dependencies appropriately, and panic if this is not the
	// case.
	Remove(name string) error

	// Check to see if a package exists in the index.
	// If the queried package is not in the index, Query will return
	// ErrNoExist.
	Query(name string) error
}

// New returns an implementation of Store based on the value of dsn
// (data source name).
//
// The data source name is expected to be a URL.
// Which Store implementation is returned depends on the URL's scheme.
// Currently, the only supported scheme is "memory", which will return an
// implementation of Store that keeps data in memory.
// "mem" and "inmem" are acceptable aliases for "memory".
func New(dsn string) (Store, error) {
	u, err := url.Parse(dsn)
	if err != nil {
		return nil, fmt.Errorf("parse url: %w", err)
	}

	switch u.Scheme {
	case "memory", "mem", "inmem":
		return NewMemoryStore(), nil
	}

	return nil, fmt.Errorf("unsupported data store scheme: %s", u.Scheme)
}
