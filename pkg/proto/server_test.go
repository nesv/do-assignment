package proto

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"math"
	"net"
	"strings"
	"testing"
	"time"
)

func TestServer(t *testing.T) {
	// Set up a test server that only listens for local traffic.
	lst, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal("new listener:", err)
	}
	defer lst.Close()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	srv, err := NewServer()
	if err != nil {
		t.Fatal("initialize store:", err)
	}

	srverrs := make(chan error)
	go func() {
		defer close(srverrs)
		if err := srv.Serve(ctx, lst); err != nil && err != context.Canceled {
			srverrs <- fmt.Errorf("serve: %w", err)
		}
	}()

	// Connect to the server.
	conn, err := Dial(lst.Addr().String())
	if err != nil {
		t.Errorf("connect to server: %v", err)
	}

	// Send requests that should return an "OK" response.
	t.Run("ok", func(t *testing.T) {
		for _, req := range []Request{
			NewRequest(CommandIndex, "ceylon"),
			NewRequest(CommandQuery, "ceylon"),
			NewRequest(CommandRemove, "ceylon"),
		} {
			resp, err := conn.Do(req)
			if err != nil {
				t.Errorf("%s: %v", req, err)
			}
			if want := OK; resp != want {
				t.Errorf("%s: want=%q got=%q", req, want, resp)
			}
		}
	})

	// Send requests that should result in a "FAIL" response.
	t.Run("fail", func(t *testing.T) {
		for _, req := range []Request{
			NewRequest(CommandIndex, "cloog", "gmp", "isl", "pkg-config"),
		} {
			resp, err := conn.Do(req)
			if err != nil {
				t.Errorf("%s: %v", req, err)
			}

			if want := Fail; resp != want {
				t.Errorf("%s: want=%q got=%q", resp, want, resp)
			}
		}

	})

	// Send requests that should result in an "ERROR" response.
	//
	// NOTE: The approach for sending requests here is different, since
	// Request.WriteTo validates the values of the struct fields before
	// marshalling the message.
	t.Run("error", func(t *testing.T) {
		conn, err := net.Dial("tcp", lst.Addr().String())
		if err != nil {
			t.Fatal(err)
		}
		defer conn.Close()

		for _, msg := range []string{
			"INDEX||",
			"NOOP||",
			"OK",
			"||",
			"|||||",
			"",
		} {
			if _, err := fmt.Fprintln(conn, msg); err != nil {
				t.Errorf("write %q: %v", msg, err)
			}
			if err := expectResponse(conn, "ERROR"); err != nil {
				t.Errorf("response to %q: %v", msg, err)
			}
		}
	})

	conn.Close()
	cancel()
	lst.Close()
	if err := <-srverrs; err != nil {
		// NOTE: This is ugly -- I know -- but there is no cleaner way
		// to accurately handle the error "(net.Listener).Accept" after
		// its Close method has been called.
		if !strings.Contains(err.Error(), "use of closed network connection") {
			t.Error(err)
		}
	}
}

// expectResponse reads a single, newline-terminated response from r, and
// checks to see if the response matches want.
// expectResponse returns a non-nil error if there was an issue reading the
// response from r, or the response does not match want.
func expectResponse(r io.Reader, want string) error {
	resp, err := readResponse(r)
	if err != nil {
		return fmt.Errorf("read response: %w", err)
	}
	if resp != want {
		return fmt.Errorf("want=%q got=%q", want, resp)
	}
	return nil
}

// readResponse reads a single, newline-terminated response from r.
func readResponse(r io.Reader) (string, error) {
	sc := bufio.NewScanner(r)
	if !sc.Scan() {
		return "", fmt.Errorf("scan: %w", sc.Err())
	}
	return sc.Text(), nil
}

func newTestServer(ctx context.Context) (net.Addr, error) {
	lst, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, err
	}

	srv, err := NewServer()
	if err != nil {
		return nil, err
	}

	go srv.Serve(ctx, lst)
	return lst.Addr(), nil
}

func TestServerTimings(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	addr, err := newTestServer(ctx)
	if err != nil {
		t.Fatal(err)
	}

	client, err := Dial(addr.String())
	if err != nil {
		t.Fatal(err)
	}
	defer client.Close()

	var (
		samples = 1000000
		timings = make([]float64, samples)
		sum     float64
		mean    float64
		stddev  float64
	)
	for i := 0; i < samples; i++ {
		start := time.Now()

		if resp, err := client.Index("ceylon"); err != nil {
			t.Error(err)
		} else if resp != OK {
			t.Errorf("unexpected response: want=%q got=%q", OK, resp)
		}
		if resp, err := client.Remove("ceylon"); err != nil {
			t.Error(err)
		} else if resp != OK {
			t.Errorf("unexpected response: want=%q got=%q", OK, resp)
		}

		duration := float64(time.Since(start))
		timings[i] = duration
		sum += duration
	}
	mean = float64(sum) / float64(samples)

	for i := 0; i < samples; i++ {
		stddev += math.Pow(float64(timings[i])-mean, 2)
	}
	stddev = math.Sqrt(stddev / float64(samples))
	t.Logf("std.dev: %s", time.Duration(stddev))

	if deviation, threshold := time.Duration(stddev), time.Millisecond; deviation >= threshold {
		t.Errorf("standard deviation exceeded threshold of %s: %s", threshold, deviation)
	}
}

func BenchmarkServer(b *testing.B) {
	// Set up the server.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	addr, err := newTestServer(ctx)
	if err != nil {
		b.Fatal(err)
	}

	client, err := Dial(addr.String())
	if err != nil {
		b.Fatal(err)
	}
	defer client.Close()

	b.ResetTimer()
	b.Run("add+remove", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			if resp, err := client.Index("ceylon"); err != nil {
				b.Error(err)
			} else if resp != OK {
				b.Errorf("unexpected response: want=%q got=%q", OK, resp)
			}

			if resp, err := client.Remove("ceylon"); err != nil {
				b.Error(err)
			} else if resp != OK {
				b.Errorf("unexpected response: want=%q got=%q", OK, resp)
			}
		}
	})
}
