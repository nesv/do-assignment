package proto

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
)

const (
	CommandIndex  = "INDEX"
	CommandRemove = "REMOVE"
	CommandQuery  = "QUERY"
)

// NewRequest creates and returns a request with the given command, package
// name, and zero or more dependencies.
//
// NewRequest does not validate the command or pkgname arguments.
// Any validation is left to Request.MarshalText and Request.UnmarshalText.
// Refer to the documentation for those functions for validation details.
func NewRequest(command, pkgname string, dependencies ...string) Request {
	deps := make([]string, len(dependencies))
	copy(deps, dependencies)

	return Request{
		Command:      command,
		Package:      pkgname,
		Dependencies: deps,
	}
}

// Request defines the structure of a request that is received by a server.
type Request struct {
	Command      string
	Package      string
	Dependencies []string
}

// ReadFrom populates the contents of m, from rdr.
func (r *Request) ReadFrom(rdr io.Reader) (int64, error) {
	txt, err := bufio.NewReader(rdr).ReadBytes('\n')
	n := int64(len(txt))
	if err != nil {
		return n, err
	}

	if err := r.UnmarshalText(txt); err != nil {
		return n, fmt.Errorf("unmarshal text: %w", err)
	}
	return n, nil
}

// UnmarshalText parses p, and populates the fields in r.
func (r *Request) UnmarshalText(p []byte) error {
	parts := bytes.SplitN(bytes.TrimRight(p, "\n"), []byte("|"), 3)
	if want, got := 3, len(parts); want != got {
		return fmt.Errorf("wanted %d fields in message, got %d", want, got)
	}

	r.Command = string(bytes.ToUpper(parts[0]))
	r.Package = string(bytes.TrimSpace(parts[1]))

	if r.Command == CommandIndex && len(parts[2]) != 0 {
		r.Dependencies = strings.Split(string(parts[2]), ",")
	}

	return nil
}

// WriteTo encodes the contents of m, adds a newline at the end of the request,
// and writes the encoded request to w.
func (r Request) WriteTo(w io.Writer) (int64, error) {
	p, err := r.MarshalText()
	if err != nil {
		return 0, err
	}

	n, err := w.Write(append(p, '\n'))
	return int64(n), err
}

// MarshalText returns the textual representation of r.
//
// NOTE: The []byte returned by MarshalText does not include a "\n" at the end.
// For rationale, please refer to the documentation for r.String.
func (r Request) MarshalText() ([]byte, error) {
	return []byte(r.String()), nil
}

// String implements the fmt.Stringer interface, for returning a string
// representation of the
//
// NOTE: The protocol outlined by this assignment terminates all messages with
// a newline "\n".
// The return value of String does not include a "\n" at the end, to allow the
// caller to control when a newline should be included.
func (r Request) String() string {
	return fmt.Sprintf("%s|%s|%s", r.Command, r.Package, strings.Join(r.Dependencies, ","))
}
