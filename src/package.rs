use crate::proto::Request;

#[derive(Debug, Clone, PartialEq)]
pub struct Package {
    name: String,
    dependencies: Option<Vec<String>>,
}

impl Package {
    pub fn new(name: &str, dependencies: Option<Vec<String>>) -> Package {
        Package {
            name: name.to_string(),
            dependencies: dependencies,
        }
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn dependencies(&self) -> Option<Vec<String>> {
        match &self.dependencies {
            Some(d) => match d.len() {
                0 => None,
                _ => Some(d.clone()),
            },
            None => None,
        }
    }
}

impl From<Request> for Package {
    fn from(r: Request) -> Self {
        Self {
            name: r.package(),
            dependencies: r.dependencies(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Package;

    #[test]
    fn package_dependencies() {
        let pkg = Package::new("a", None);
        let deps = pkg.dependencies();
        assert!(deps.is_none());

        let pkg = Package::new("a", Some(vec![]));
        let deps = pkg.dependencies();
        assert!(deps.is_none());
    }
}
