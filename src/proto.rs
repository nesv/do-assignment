//! The `proto` module provides the `Request` and `Response` types for
//! encoding and decoding client/server messages over a connection.
//!
//! # Requests
//!
//! Requests take the following form:
//!
//! ```ignore
//! COMMAND|package_name|[dependency[,...]]\n
//! ```
//!
//! The command, and package name are always mandatory, and the request must
//! be terminated with a newline character, `\n`.
//! Zero or more dependencies may be specified in a request, and must be
//! separated by commas.
//!
//! Note that dependencies are only used for `INDEX` commands.
//! It is valid to send an `INDEX` request with zero dependencies.
//! If an `INDEX` request specifies one or more dependencies, each of those
//! dependencies must already be indexed.
//!
//! For `REMOVE` and `QUERY` commands, dependencies are ignored.
//!
//! # Responses
//!
//! A server will respond to a request with one of the following,
//! newline-terminated messages:
//!
//! - `OK` &mdash; everything went well;
//! - `FAIL` &mdash; there was an issue completing the request;
//! - `ERROR` &mdash; the request did not contain a valid command, the
//!   package name was missing, or the request was otherwise malfomed.

use std::{
    fmt,
    io::{Error, ErrorKind},
    str::FromStr,
};

/// A request that can be sent from a client, to the server.
#[derive(Clone)]
pub struct Request {
    cmd: Command,
    pkg: String,
    deps: Option<Vec<String>>,
}

impl Request {
    pub fn command(&self) -> Command {
        self.cmd.clone()
    }

    pub fn package(&self) -> String {
        self.pkg.clone()
    }

    pub fn dependencies(&self) -> Option<Vec<String>> {
        match &self.deps {
            Some(deps) => match deps.len() {
                0 => None,
                _ => {
                    let v = deps.iter().map(|s| s.clone()).collect();
                    Some(v)
                }
            },
            None => None,
        }
    }
}

impl FromStr for Request {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = value.split('|').map(|s| s.trim()).collect();
        if parts.len() < 3 {
            return Err(Error::new(
                ErrorKind::Other,
                format!("malformed request: {:?}", value),
            ));
        }

        let cmd: Command = parts[0].parse()?;
        let pkg = parts[1].to_string();

        let mut deps: Option<Vec<String>> = None;
        if parts[2].len() > 0 {
            let v: Vec<String> = parts[2].split(",").map(|s| s.to_string()).collect();
            deps = Some(v);
        }

        Ok(Self { cmd, pkg, deps })
    }
}

impl fmt::Display for Request {
    /// Formats the value using the given formatter.
    ///
    /// This implementation does not append a trailing newline to the formatted
    /// value.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.deps {
            Some(deps) => match deps.len() {
                0 => write!(f, "{}|{}|", self.cmd, self.pkg),
                _ => write!(f, "{}|{}|{}", self.cmd, self.pkg, deps.join(",")),
            },
            None => write!(f, "{}|{}|", self.cmd, self.pkg),
        }
    }
}

/// Command enumerates all of the supported commands that can be provided in a
/// request.
///
/// If an invalid command is parsed from a request message, the returned
/// variant will be `Command::Unknown`.
/// This is to allow the caller to distinguish between parsing errors, and
/// I/O errors.
#[derive(Clone, PartialEq, Debug)]
pub enum Command {
    Unknown(String),
    Index,
    Remove,
    Query,
}

impl FromStr for Command {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let value = value.to_uppercase();
        match value.as_str() {
            "INDEX" => Ok(Self::Index),
            "REMOVE" => Ok(Self::Remove),
            "QUERY" => Ok(Self::Query),
            _ => Ok(Self::Unknown(value.to_string())),
        }
    }
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Unknown(s) => write!(f, "{}", s),
            Self::Index => write!(f, "INDEX"),
            Self::Remove => write!(f, "REMOVE"),
            Self::Query => write!(f, "QUERY"),
        }
    }
}

/// A response sent from the server, to a client.
#[derive(PartialEq)]
pub enum Response {
    Unknown(String),
    OK,
    Fail,
    Error,
}

impl FromStr for Response {
    type Err = Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        let value = value.to_uppercase();
        match value.as_str() {
            "OK" => Ok(Self::OK),
            "FAIL" => Ok(Self::Fail),
            "ERROR" => Ok(Self::Error),
            _ => Ok(Self::Unknown(value.to_string())),
        }
    }
}

impl fmt::Display for Response {
    /// Formats the value using the given formatter.
    ///
    /// This implementation does not append a trailing newline to the formatted
    /// value.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Unknown(s) => write!(f, "{}", s),
            Self::OK => write!(f, "OK"),
            Self::Fail => write!(f, "FAIL"),
            Self::Error => write!(f, "ERROR"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Command, Request};

    #[test]
    fn command_parse() {
        let cmd: Command = "INDEX".parse().unwrap();
        assert_eq!(cmd, Command::Index);

        let cmd: Command = "REMOVE".parse().unwrap();
        assert_eq!(cmd, Command::Remove);

        let cmd: Command = "QUERY".parse().unwrap();
        assert_eq!(cmd, Command::Query);

        let cmd: Command = "FOOBAR".parse().unwrap();
        assert_eq!(cmd, Command::Unknown("FOOBAR".to_string()));
    }

    #[test]
    fn command_parse_lcase() {
        let cmd: Command = "query".parse().unwrap();
        assert_eq!(cmd, Command::Query);
    }

    #[test]
    fn request_parse() {
        let req: Request = "INDEX|cloog|\n".parse().unwrap();
        assert_eq!(req.command(), Command::Index);
        assert_eq!(req.package(), "cloog".to_string());
        assert!(req.dependencies().is_none());

        let req: Request = "INDEX|cloog|gmp,isl,pkg-config\n".parse().unwrap();
        assert_eq!(req.command(), Command::Index);
        assert_eq!(req.package(), "cloog".to_string());
        assert_eq!(
            req.dependencies(),
            Some(vec![
                "gmp".to_string(),
                "isl".to_string(),
                "pkg-config".to_string()
            ])
        );
    }
}
