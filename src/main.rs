use do_assignment::{
    index::{Ephemeral, PackageIndex},
    package::Package,
    proto::{Command, Request, Response},
};
use std::{
    io::{stderr, BufRead, BufReader, BufWriter, Result, Write},
    net::{TcpListener, ToSocketAddrs},
    sync::{Arc, Mutex},
    thread,
};

/// The maximum size of the buffer we will use to receive messages from
/// clients.
const RECV_BUFSIZE: usize = 1 << 20;

fn main() -> Result<()> {
    let server = Server::new();
    server.start("0.0.0.0:8080")
}

struct Server {
    index: Arc<Mutex<Ephemeral>>,
}

impl Server {
    fn new() -> Self {
        Self {
            index: Arc::new(Mutex::new(Ephemeral::new())),
        }
    }

    fn start<A: ToSocketAddrs>(&self, addr: A) -> Result<()> {
        // This vec holds the join handles for each thread we spin up
        // (one thread per client connection).
        let mut children = vec![];

        let listener = TcpListener::bind(addr)?;
        for stream in listener.incoming() {
            let stream = stream?;
            let peer_addr = stream.peer_addr().unwrap();
            let thread_name = format!("client connection: {}", peer_addr);
            let index = Arc::clone(&self.index);
            let mut errlog = BufWriter::new(stderr());

            children.push(thread::Builder::new().name(thread_name).spawn(move || {
                writeln!(errlog, "{}: client connected", peer_addr).unwrap();

                let mut reader = BufReader::with_capacity(RECV_BUFSIZE, &stream);
                loop {
                    // Read from the connection.
                    let mut buf = String::with_capacity(RECV_BUFSIZE);
                    if let Err(e) = reader.read_line(&mut buf) {
                        writeln!(errlog, "{}: read: {}", peer_addr, e).unwrap();
                        break;
                    }

                    // Parse the request.
                    let req: Request = match buf.parse() {
                        Ok(m) => m,
                        Err(_) => {
                            if let Err(_) = writeln!(&stream, "{}", Response::Error) {
                                break;
                            }
                            continue;
                        }
                    };

                    // Make sure the package name was specified in the
                    // message.
                    if req.package().len() == 0 {
                        if let Err(_) = writeln!(&stream, "{}", Response::Error) {
                            break;
                        }
                        continue;
                    }

                    // Figure out what the client wants to do.
                    match req.command() {
                        Command::Unknown(cmd) => {
                            // Return an ERROR response.
                            writeln!(errlog, "{}: unknown command: {}", peer_addr, cmd).unwrap();
                            if let Err(_) = writeln!(&stream, "{}", Response::Error) {
                                break;
                            }
                            continue;
                        }

                        Command::Index => match index.lock() {
                            Ok(ref mut index) => {
                                if let Err(err) = index.index(Package::from(req.clone())) {
                                    writeln!(errlog, "{}: {}: {}", peer_addr, req, err).unwrap();
                                    if let Err(_) = writeln!(&stream, "{}", Response::Fail) {
                                        break;
                                    }
                                    continue;
                                }
                            }
                            Err(e) => {
                                panic!("{}", e);
                            }
                        },

                        Command::Remove => match index.lock() {
                            Ok(ref mut index) => {
                                if let Err(err) = index.remove(&req.package()) {
                                    writeln!(errlog, "{}: {}: {}", peer_addr, req, err).unwrap();
                                    if let Err(_) = writeln!(&stream, "{}", Response::Fail) {
                                        break;
                                    }
                                    continue;
                                }
                            }
                            Err(e) => {
                                panic!("{}", e);
                            }
                        },

                        Command::Query => match index.lock() {
                            Ok(ref index) => match index.query(&req.package()) {
                                Ok(pkg) => {
                                    if pkg.is_none() {
                                        if let Err(_) = writeln!(&stream, "{}", Response::Fail) {
                                            break;
                                        }
                                        continue;
                                    }
                                }

                                Err(err) => {
                                    writeln!(errlog, "{}: {}: {}", peer_addr, req, err).unwrap();
                                    if let Err(_) = writeln!(&stream, "{}", Response::Fail) {
                                        break;
                                    }
                                    continue;
                                }
                            },
                            Err(e) => {
                                panic!("{}", e);
                            }
                        },
                    }

                    if let Err(_) = writeln!(&stream, "{}", Response::OK) {
                        break;
                    }
                }
                writeln!(errlog, "Client disconnected: {}", peer_addr).unwrap();
                errlog.flush().unwrap();
            }));
        }

        for child in children {
            let _ = child.unwrap().join();
        }
        Ok(())
    }
}
