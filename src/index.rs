//! Provides the trait for implementing a package index, as well as an
//! in-memory implementation.

use crate::package::Package;
use std::{
    collections::{HashMap, HashSet},
    error, fmt,
};

#[derive(Debug)]
pub enum Error {
    EmptyPackageName,
    EmptyDependencyName,
    NotIndexed(String),
    CircularDependency(String, String),
    MissingDependency(String, String),
    Missing(String),
    MissingReverseDependency(String, String),
    NoReverseDependencies(String),
    DependencyNotIndexed(String),
    HasDependants(Vec<String>),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::EmptyPackageName => write!(f, "empty package name"),
            Self::EmptyDependencyName => write!(f, "empty dependency name"),
            Self::NotIndexed(name) => write!(f, "package {:?} is not indexed", name),
            Self::CircularDependency(pkg, dep) => {
                write!(f, "circular dependency detected: {:?} <-> {:?}", pkg, dep)
            }
            Self::MissingDependency(pkg, dep) => {
                write!(f, "{:?} is not a dependency of {:?}", dep, pkg)
            }
            Self::Missing(pkg) => write!(f, "package {:?} does not exist", pkg),
            Self::MissingReverseDependency(pkg, dep) => {
                write!(f, "{:?} is not a reverse dependency of {:?}", dep, pkg)
            }
            Self::NoReverseDependencies(pkg) => write!(f, "no reverse dependencies for {:?}", pkg),
            Self::DependencyNotIndexed(dep) => write!(f, "dependency {:?} not indexed", dep),
            Self::HasDependants(pkgs) => write!(f, "package depended on by {}", pkgs.join(",")),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

/// The `PackageIndex` trait defines the methods that must be implemented on any
/// type that stores an index of packages, and their dependencies.
pub trait PackageIndex {
    /// Index adds a package to the index.
    fn index(&mut self, pkg: Package) -> Result<(), Error>;

    /// Remove a package from the index.
    /// If any packages depend on `pkgname`, this method will fail.
    fn remove(&mut self, pkgname: &str) -> Result<(), Error>;

    /// Check to see if a package is in the index.
    fn query(&self, pkgname: &str) -> Result<Option<Package>, Error>;
}

/// `Ephemeral` is an implementation of a [`PackageIndex`] that only lives
/// in memory, and has no means of persisting data.
///
/// Despite this limitation, `Ephemeral` is a fully-functional implementation
/// that includes:
///
/// - circular dependency detection;
/// - reverse dependency management.
///
/// [`PackageIndex`]: trait.PackageIndex.html
///
pub struct Ephemeral {
    packages: HashMap<String, HashSet<String>>,
    rev_deps: HashMap<String, HashSet<String>>,
}

impl Ephemeral {
    pub fn new() -> Ephemeral {
        Self {
            packages: HashMap::new(),
            rev_deps: HashMap::new(),
        }
    }

    /// Returns the packages that depend on `pkgname`.
    fn dependants(&self, pkgname: &str) -> Option<Vec<String>> {
        if let Some(dependants) = self.rev_deps.get(pkgname) {
            if dependants.len() == 0 {
                return None;
            }

            let v: Vec<String> = dependants.iter().map(|s| s.clone()).collect();
            return Some(v);
        }
        None
    }

    #[cfg(test)]
    fn dependencies(&self, pkgname: &str) -> Option<Vec<String>> {
        match self.packages.get(pkgname) {
            Some(deps) => match deps.len() {
                0 => None,
                _ => {
                    let v: Vec<String> = deps.iter().map(|s| s.clone()).collect();
                    Some(v)
                }
            },
            None => None,
        }
    }

    fn update_dependencies(&mut self, pkg: Package) -> Result<(), Error> {
        let name = pkg.name();
        match self.packages.get(&name) {
            Some(old_deps) => {
                let mut to_add: Vec<String> = vec![];
                let mut to_remove: Vec<String> = vec![];

                match pkg.dependencies() {
                    Some(new_deps) => {
                        // The package has dependencies, so figure out which ones
                        // we will need to add, and remove.
                        for dep in &new_deps {
                            if !old_deps.contains(dep) {
                                to_add.push(dep.to_string());
                            }
                        }

                        for dep in old_deps {
                            match new_deps.iter().position(|d| d == dep) {
                                Some(_) => {}
                                None => {
                                    to_remove.push(dep.to_string());
                                }
                            }
                        }
                    }
                    None => {
                        // The package does not have any dependencies, so any
                        // old dependencies should be removed.
                        to_remove = old_deps.iter().map(|d| d.to_string()).collect();
                    }
                }

                for dep in to_add {
                    self.add_dependency(&name, &dep)?;
                }
                for dep in to_remove {
                    self.remove_dependency(&name, &dep)?;
                }

                Ok(())
            }

            None => {
                // The package does not exist in the index.
                Err(Error::NotIndexed(name))
            }
        }
    }

    /// Adds a dependency for `pkgname` to `dependency`, as well as a reverse
    /// dependency from `dependency` to `pkgname`.
    /// If `pkgname` does not exist in the index, it will be added.
    ///
    /// # Errors
    ///
    /// Returns [`Err`] if the dependency does not exist in the index.
    ///
    /// [`Err`]: enum.Result.html#variant.Err
    ///
    fn add_dependency(&mut self, pkgname: &str, dependency: &str) -> Result<(), Error> {
        if pkgname.len() == 0 {
            return Err(Error::EmptyPackageName);
        }
        if dependency.len() == 0 {
            return Err(Error::EmptyDependencyName);
        }

        // Make sure we are not introducing a circular dependency.
        // This will also ensure our dependency exists.
        match self.packages.get(dependency) {
            Some(dependencies) => {
                // Check for a circular dependency.
                if dependencies.contains(pkgname) {
                    return Err(Error::CircularDependency(
                        pkgname.to_string(),
                        dependency.to_string(),
                    ));
                }
            }
            None => {
                // Our dependency does not exist.
                return Err(Error::NotIndexed(dependency.to_string()));
            }
        }

        // Add the dependency.
        // If the package does not exist in the index, this will add it.
        match self.packages.get_mut(pkgname) {
            Some(dependencies) => {
                dependencies.insert(dependency.to_string());
            }
            None => {
                let mut dependencies: HashSet<String> = HashSet::new();
                dependencies.insert(dependency.to_string());
                self.packages.insert(pkgname.to_string(), dependencies);
            }
        }

        // Add the reverse dependency.
        self.add_reverse_dependency(pkgname, dependency);

        Ok(())
    }

    /// Stores the reverse dependency mapping from `dependency` to `pkgname`.
    /// If `dependency` is not stored in the index's reverse dependencies, an
    /// entry will be created.
    fn add_reverse_dependency(&mut self, pkgname: &str, dependency: &str) {
        match self.rev_deps.get_mut(dependency) {
            Some(rdeps) => {
                rdeps.insert(pkgname.to_string());
            }
            None => {
                let mut rdeps: HashSet<String> = HashSet::new();
                rdeps.insert(pkgname.to_string());
                self.rev_deps.insert(dependency.to_string(), rdeps);
            }
        }
    }

    /// Removes a dependency from `pkgname` to `dependency`, and removes the
    /// reverse dependency from `dependency` to `pkgname`.
    fn remove_dependency(&mut self, pkgname: &str, dependency: &str) -> Result<(), Error> {
        if pkgname.len() == 0 {
            return Err(Error::EmptyPackageName);
        }
        if dependency.len() == 0 {
            return Err(Error::EmptyDependencyName);
        }

        match self.packages.get_mut(pkgname) {
            Some(dependencies) => {
                if !dependencies.remove(dependency) {
                    return Err(Error::MissingDependency(
                        pkgname.to_string(),
                        dependency.to_string(),
                    ));
                }
            }
            None => {
                return Err(Error::Missing(pkgname.to_string()));
            }
        }

        self.remove_reverse_dependency(pkgname, dependency)?;
        Ok(())
    }

    fn remove_reverse_dependency(&mut self, pkgname: &str, dependency: &str) -> Result<(), Error> {
        match self.rev_deps.get_mut(dependency) {
            Some(rdeps) => match rdeps.remove(pkgname) {
                true => Ok(()),
                false => Err(Error::MissingReverseDependency(
                    pkgname.to_string(),
                    dependency.to_string(),
                )),
            },
            None => Err(Error::NoReverseDependencies(dependency.to_string())),
        }
    }
}

impl PackageIndex for Ephemeral {
    fn index(&mut self, pkg: Package) -> Result<(), Error> {
        // Make sure there is a non-empty package name.
        if pkg.name().len() == 0 {
            return Err(Error::EmptyPackageName);
        }

        // Make sure any dependencies are already in the index.
        if let Some(deps) = pkg.dependencies() {
            for dep in &deps {
                if !self.packages.contains_key(dep) {
                    return Err(Error::DependencyNotIndexed(dep.to_string()));
                }
            }
        }

        // If the package does not exist in the index, just add it.
        let name = pkg.name();
        match self.packages.contains_key(&name) {
            true => {
                self.update_dependencies(pkg)?;
            }

            // The package does not exist in the index.
            false => match pkg.dependencies() {
                Some(deps) => {
                    for dep in deps {
                        self.add_dependency(&name, &dep)?;
                    }
                }
                None => {
                    self.packages.insert(name, HashSet::new());
                }
            },
        }

        Ok(())
    }

    fn remove(&mut self, pkgname: &str) -> Result<(), Error> {
        if pkgname.len() == 0 {
            return Err(Error::EmptyPackageName);
        }

        if !self.packages.contains_key(pkgname) {
            return Ok(());
        }

        // Make sure this package has no dependants.
        if let Some(pkgs) = self.dependants(pkgname) {
            return Err(Error::HasDependants(pkgs));
        }

        // Nothing depends on this package.
        if let Some(dependencies) = self.packages.remove(pkgname) {
            for dep in dependencies {
                self.remove_reverse_dependency(pkgname, &dep)?;
            }
        }
        Ok(())
    }

    fn query(&self, pkgname: &str) -> Result<Option<Package>, Error> {
        if pkgname.len() == 0 {
            return Err(Error::EmptyPackageName);
        }

        if let Some(deps) = self.packages.get(pkgname) {
            if deps.len() == 0 {
                let pkg = Package::new(pkgname, None);
                return Ok(Some(pkg));
            }

            let v: Vec<String> = deps.iter().map(|s| s.to_string()).collect();
            let pkg = Package::new(pkgname, Some(v));
            return Ok(Some(pkg));
        }

        Ok(None)
    }
}

#[cfg(test)]
mod tests {
    use super::{Ephemeral, Package, PackageIndex};

    #[test]
    fn ephemeral_index() {
        let mut i = Ephemeral::new();
        assert!(i.index(Package::new("ceylon", None)).is_ok());
    }

    #[test]
    fn ephemeral_index_with_dependencies() {
        let pkgs = vec![
            Package::new("gmp", None),
            Package::new("isl", None),
            Package::new("pkg-config", None),
            Package::new(
                "cloog",
                Some(vec![
                    "gmp".to_string(),
                    "isl".to_string(),
                    "pkg-config".to_string(),
                ]),
            ),
        ];

        let mut i = Ephemeral::new();
        for pkg in pkgs {
            dbg!(pkg.name());
            assert!(i.index(pkg).is_ok());
        }
    }

    #[test]
    fn ephemeral_index_no_package_name() {
        let mut i = Ephemeral::new();
        assert!(i.index(Package::new("", None)).is_err());
    }

    #[test]
    fn ephemeral_index_unindexed_dependencies() {
        let mut i = Ephemeral::new();
        let pkg = Package::new(
            "cloog",
            Some(vec![
                "gmp".to_string(),
                "isl".to_string(),
                "pkg-config".to_string(),
            ]),
        );
        assert!(i.index(pkg).is_err());
    }

    #[test]
    fn ephemeral_dependants() {
        let a = Package::new("a", None);
        let b = Package::new("b", Some(vec!["a".to_string()]));

        let mut i = Ephemeral::new();
        assert!(i.index(a).is_ok());
        assert!(i.index(b).is_ok());

        assert_eq!(i.dependants("a"), Some(vec!["b".to_string()]));
    }

    // This test checks to make sure circular dependencies cannot be introduced
    // to the package index.
    #[test]
    fn ephemeral_index_circular_dependencies() {
        // Index two packages.
        // The second package should depend on the first package.
        // Update the first package to depend on the second package, thus
        // introducing a circular dependency.
        // This should fail.
        let a = Package::new("a", None);
        let b = Package::new("b", Some(vec!["a".to_string()]));

        let mut i = Ephemeral::new();
        assert!(i.index(a).is_ok());
        assert!(i.index(b).is_ok());

        let a = Package::new("a", Some(vec!["b".to_string()]));
        assert!(i.index(a).is_err());
    }

    #[test]
    fn ephemeral_index_update_dependencies() {
        let pkgs = vec![
            Package::new("gmp", None),
            Package::new("isl", None),
            Package::new("pkg-config", None),
            Package::new(
                "cloog",
                Some(vec![
                    "gmp".to_string(),
                    "isl".to_string(),
                    "pkg-config".to_string(),
                ]),
            ),
        ];

        let mut i = Ephemeral::new();
        for pkg in pkgs {
            dbg!(pkg.name());
            assert!(i.index(pkg).is_ok());
        }

        match i.dependencies("cloog") {
            Some(deps) => {
                if deps.len() != 3 {
                    panic!("package should have 3 dependencies, has {}", deps.len());
                }
            }
            None => {
                panic!("package should have dependencies");
            }
        }

        // Re-index "cloog", removing all of its dependencies, and verify
        // all dependencies are gone.
        let cloog = Package::new("cloog", None);
        assert!(i.index(cloog).is_ok());
        assert!(i.dependencies("cloog").is_none());
    }

    #[test]
    fn ephemeral_remove() {
        let mut i = Ephemeral::new();
        assert!(i.index(Package::new("ceylon", None)).is_ok());
        assert!(i.remove("ceylon").is_ok());

        // Verify the package has been removed, by trying to query it.
        let res = i.query("ceylon");
        assert!(res.is_ok());
        assert!(res.unwrap().is_none());
    }

    #[test]
    fn ephemeral_remove_no_package_name() {
        let mut i = Ephemeral::new();
        assert!(i.remove("").is_err());
    }

    #[test]
    fn ephemeral_remove_with_dependants() {
        // Index two packages, and have the second package depend on the first
        // one.
        // Then, attempt to remove the first package.
        // This should fail.
        let a = Package::new("a", None);
        let b = Package::new("b", Some(vec![a.name()]));

        let mut i = Ephemeral::new();
        assert!(i.index(a).is_ok());
        assert!(i.index(b).is_ok());

        assert!(i.remove("a").is_err());

        // Update the second package, removing its dependency on the first
        // package, and attempt to remove the first package again.
        // This second attempt should pass.
        let b = Package::new("b", None);
        assert!(i.index(b).is_ok());
        assert!(i.remove("a").is_ok());
    }

    #[test]
    fn ephemeral_query() {
        let pkg = Package::new("ceylon", None);
        let mut i = Ephemeral::new();
        assert!(i.index(pkg.clone()).is_ok());

        let other = i.query(&pkg.name()).unwrap().unwrap();
        if other != pkg {
            panic!("want={:?} got={:?}", pkg, other);
        }
    }

    #[test]
    fn ephemeral_query_no_package_name() {
        let i = Ephemeral::new();
        assert!(i.query("").is_err());
    }
}
