package store

import (
	"errors"
	"fmt"
	"sync"

	"assignment/pkg/strutil"
)

// NewMemoryStore returns an in-memory data store for indexing packages.
func NewMemoryStore() Store {
	return &inmem{
		pkgs: make(map[string][]string),
		rev:  make(map[string][]string),
	}
}

type inmem struct {
	mu sync.RWMutex

	// Mapping of package name -> dependencies.
	pkgs map[string][]string

	// Stores a reverse mapping of package name -> dependants
	// (reverse-dependencies).
	// Performing a lookup on this map, for any given package name,
	// will return a list of packages that depend on it.
	rev map[string][]string
}

func (m *inmem) withRLock(fn func() error) error {
	m.mu.RLock()
	defer m.mu.RUnlock()
	return fn()
}

func (m *inmem) withLock(fn func() error) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	return fn()
}

func (m *inmem) Index(name string, dependencies ...string) error {
	if name == "" {
		return errors.New("zero-length package name")
	}

	return m.withLock(func() error {
		// Check to see if this package has already been indexed.
		// If it has, we will update its dependency list, which will
		// also require us to update the each dependency's
		// reverse-dependency listing.
		odeps, ok := m.pkgs[name]
		if !ok {
			// This package is not already indexed.
			// Ultra happy path.
			return m.indexPackage(name, dependencies)
		}

		// The package already exists in the index.
		//
		// From this point, we are going to figure out if the
		// dependencies have changed, and make the changes to the
		// dependency and reverse-dependency mappings.
		var (
			added   []string
			removed []string
		)
		for _, d := range dependencies {
			// Skip over any zero-length dependency names.
			if d == "" {
				continue
			}

			if strutil.SliceIndex(odeps, d) == -1 {
				added = append(added, d)
			}
		}
		for _, d := range odeps {
			if strutil.SliceIndex(dependencies, d) == -1 {
				removed = append(removed, d)
			}
		}

		// If no dependencies have been added or removed, there is
		// nothing to change, so short-circuit the rest of this
		// function.
		if len(added) == 0 && len(removed) == 0 {
			return nil
		}

		// Check to make sure all of the new dependencies are indexed
		// and no circular dependencies are being introduced.
		if err := m.chkdeps(name, added); err != nil {
			return err
		}

		// Update the package's dependencies.
		m.pkgs[name] = make([]string, len(dependencies))
		copy(m.pkgs[name], dependencies)

		// For any dependency that has been removed, remove package
		// "name" from the dependency's reverse-dependency list.
		for _, dep := range removed {
			dd, ok := m.rev[dep]
			if !ok {
				// There is no reverse dependency mapping?!
				panic(fmt.Sprintf("rm rdeps: no rdep mapping for %q", dep))
			}

			if n := strutil.SliceIndex(dd, name); n != -1 {
				m.rev[dep] = append(dd[:n], dd[n+1:]...)
			}
		}

		// Add reverse dependencies for each new dependency.
		for _, dep := range added {
			rd, _ := m.rev[dep]
			if strutil.SliceIndex(rd, name) == -1 {
				m.rev[dep] = append(rd, name)
			}
		}

		return nil
	})
}

// indexPackage adds the package name to the index, along with its
// dependencies.
// All of the dependencies must already be indexed; any dependency that is
// not indexed will result in a non-nil error being returned.
//
// The reverse-dependencies for each dependency, to package "name",
// will be updated as well.
//
// A write-lock must be held before indexPackage is called.
func (m *inmem) indexPackage(name string, dependencies []string) error {
	if err := m.chkdeps(name, dependencies); err != nil {
		return err
	}

	m.pkgs[name] = make([]string, len(dependencies))
	copy(m.pkgs[name], dependencies)

	// For each dependency, store the package specified by "name"
	// as a dependant.
	for _, dep := range dependencies {
		dependants, ok := m.rev[dep]
		if !ok {
			m.rev[dep] = []string{name}
			continue
		}
		dependants = append(dependants, name)
		m.rev[dep] = dependants
	}
	return nil
}

// chkdeps checks the dependencies for the package name, to ensure that all
// dependencies are already in the index, and that there are no circular
// dependencies.
//
// A write-lock must be held before chkdeps is called.
func (m *inmem) chkdeps(name string, dependencies []string) error {
	for _, dep := range dependencies {
		dd, indexed := m.pkgs[dep]
		if !indexed {
			return fmt.Errorf("dependency not indexed: %s", dep)
		}

		if strutil.SliceIndex(dd, name) != -1 {
			return fmt.Errorf("circular dependency detected: %s <-> %s", dep, name)
		}
	}
	return nil
}

func (m *inmem) Remove(name string) error {
	if name == "" {
		return nil
	}

	return m.withLock(func() error {
		// If the package is not in the index, return early.
		deps, indexed := m.pkgs[name]
		if !indexed {
			return nil
		}

		// The package is indexed.
		// Check to make sure nothing depends on this package;
		// that there are no reverse dependencies.
		if rdeps, _ := m.rev[name]; len(rdeps) > 0 {
			return fmt.Errorf("depended on by: %v", rdeps)
		}

		// Package "name" has no reverse dependencies.
		// For each of the package's dependency, remove the package
		// from each dependency's reverse-dependencies list.
		for _, dep := range deps {
			prds, ok := m.rev[dep]
			if !ok {
				// This is just a safeguard, but should never
				// be tripped so long as Index is implemented
				// correctly.
				panic(fmt.Sprintf("no reverse dependencies stored for %q", dep))
			}

			// Find the slice index for "name", and remove it from
			// the dependency's reverse-dependency list.
			n := strutil.SliceIndex(prds, name)
			if n == -1 {
				// This is another safeguard.
				// Again, this should never be tripped, but we
				// are going to guard against it all the same.
				panic(fmt.Sprintf("%q not in reverse dependencies for %q", name, dep))
			}

			// Remove package "name" from the dependency's
			// reverse-dependencies.
			m.rev[dep] = append(prds[:n], prds[n+1:]...)
		}

		// Remove the package from the index mapping, and the
		// reverse-dependencies mapping.
		delete(m.pkgs, name)
		delete(m.rev, name)
		return nil
	})
}

func (m *inmem) Query(name string) error {
	if name == "" {
		return nil
	}

	return m.withRLock(func() error {
		if _, indexed := m.pkgs[name]; !indexed {
			return ErrNoExist
		}
		return nil
	})
}

func (m *inmem) Close() error {
	return nil
}
