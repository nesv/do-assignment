package proto

import (
	"bytes"
	"strings"
	"testing"
)

func TestRequest(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  Request
		fail  bool

		// Skip this test case if it only pertains to
		// decoding a request.
		decodeOnly bool
	}{
		{
			name:  "IndexWithDeps",
			input: "INDEX|cloog|gmp,isl,pkg-config\n",
			want:  NewRequest(CommandIndex, "cloog", "gmp", "isl", "pkg-config"),
		},
		{
			name:  "IndexNoDeps",
			input: "INDEX|ceylon|\n",
			want:  NewRequest(CommandIndex, "ceylon"),
		},
		{
			name:  "Remove",
			input: "REMOVE|cloog|\n",
			want:  NewRequest(CommandRemove, "cloog"),
		},
		{
			name:  "Query",
			input: "QUERY|cloog|\n",
			want:  NewRequest(CommandQuery, "cloog"),
		},
		{
			name:  "UnknownCommand",
			input: "NOOP|asdf|\n",
			want:  NewRequest("NOOP", "asdf"),
		},
		{
			name:  "NoPackageName",
			input: "QUERY||\n",
			want:  NewRequest(CommandQuery, ""),
		},
		{
			name:       "WrongNumFields",
			input:      "HELO|\n",
			want:       NewRequest("HELO", ""),
			fail:       true,
			decodeOnly: true,
		},
	}

	t.Run("ReadFrom", func(t *testing.T) {
		t.Parallel()
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				var req Request
				if _, err := req.ReadFrom(strings.NewReader(tt.input)); err != nil && !tt.fail {
					t.Fatal(err)
				} else if err == nil && tt.fail {
					t.Fatal("expected non-nil error")
				} else if err != nil && tt.fail {
					return
				}

				if want, got := tt.want.Command, req.Command; want != got {
					t.Errorf("bad command: want=%q got=%q", want, got)
				}
				if want, got := tt.want.Package, req.Package; want != got {
					t.Errorf("bad package name: want=%q got=%q", want, got)
				}
				if want, got := len(tt.want.Dependencies), len(req.Dependencies); want != got {
					t.Errorf("wrong number of dependencies: want=%d got=%d (%v)", want, got, req.Dependencies)
				}
				for i, v := range tt.want.Dependencies {
					if req.Dependencies[i] != v {
						t.Errorf("dependency %d: want=%q got=%q", i+1, v, req.Dependencies[i])
					}
				}
			})
		}
	})

	t.Run("UnmarshalText", func(t *testing.T) {
		t.Parallel()
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				var req Request
				if err := req.UnmarshalText([]byte(tt.input)); err != nil && !tt.fail {
					t.Fatal(err)
				} else if err == nil && tt.fail {
					t.Fatal("expected non-nil error")
				} else if err != nil && tt.fail {
					return
				}

				if want, got := tt.want.Command, req.Command; want != got {
					t.Errorf("bad command: want=%q got=%q", want, got)
				}
				if want, got := tt.want.Package, req.Package; want != got {
					t.Errorf("bad package name: want=%q got=%q", want, got)
				}
				if want, got := len(tt.want.Dependencies), len(req.Dependencies); want != got {
					t.Errorf("wrong number of dependencies: want=%d got=%d (%v)", want, got, req.Dependencies)
				}
				for i, v := range tt.want.Dependencies {
					if req.Dependencies[i] != v {
						t.Errorf("dependency %d: want=%q got=%q", i+1, v, req.Dependencies[i])
					}
				}
			})
		}
	})

	// For these next, few tests, we are going to reuse the existing test
	// cases, but swap the use of "input" and "want".

	t.Run("WriteTo", func(t *testing.T) {
		t.Parallel()
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				if tt.decodeOnly {
					t.Skip()
				}

				var buf bytes.Buffer
				n, err := tt.want.WriteTo(&buf)
				if err != nil && !tt.fail {
					t.Fatal(err)
				} else if err != nil && tt.fail {
					return
				} else if err == nil && tt.fail {
					t.Fatal("expected non-nil error")
				}

				if want, got := len(tt.input), int(n); want != got {
					t.Errorf("bytes written=%d, want=%d", got, want)
				}

				if want, got := tt.input, buf.String(); want != got {
					t.Errorf("want=%q got=%q", want, got)
				}
			})
		}
	})

	t.Run("MarshalText", func(t *testing.T) {
		t.Parallel()
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				if tt.decodeOnly {
					t.Skip()
				}

				got, err := tt.want.MarshalText()
				if err != nil && !tt.fail {
					t.Fatal(err)
				} else if err != nil && tt.fail {
					return
				} else if err == nil && tt.fail {
					t.Fatal("expected non-nil error")
				}

				// Append a "\n" to got, since MarshalText
				// does not do that.
				got = append(got, '\n')

				if !bytes.Equal([]byte(tt.input), got) {
					t.Errorf("want=%q got=%q", tt.input, got)
				}
			})
		}
	})

	t.Run("String", func(t *testing.T) {
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				if tt.decodeOnly {
					t.Skip()
				}

				want, got := tt.input, tt.want.String()
				got += "\n"
				if want != got && !tt.fail {
					t.Errorf("want=%q got=%q", want, got)
				}
			})
		}
	})
}

func BenchmarkRequest(b *testing.B) {
	b.Run("ReadFrom", func(b *testing.B) {
		b.Run("index nodeps", func(b *testing.B) {
			var (
				txt = "INDEX|cloog|\n"
				req Request
			)
			b.ResetTimer()
			for i := 0; i < b.N; i++ {
				if _, err := req.ReadFrom(strings.NewReader(txt)); err != nil {
					b.Error(err)
				}
			}
		})

		b.Run("index 3 deps", func(b *testing.B) {
			var (
				txt = "INDEX|cloog|gmp,isl,pkg-config\n"
				req Request
			)
			b.ResetTimer()
			for i := 0; i < b.N; i++ {
				if _, err := req.ReadFrom(strings.NewReader(txt)); err != nil {
					b.Error(err)
				}
			}
		})
	})

	b.Run("UnmarshalText", func(b *testing.B) {
		var (
			txt = []byte("INDEX|cloog|gmp,isl,pkg-config\n")
			req Request
		)
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			if err := req.UnmarshalText(txt); err != nil {
				b.Error(err)
			}
		}
	})
}
