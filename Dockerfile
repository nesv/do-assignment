ARG VERSION=dev

FROM golang:latest AS build
WORKDIR /go/src/assignment
COPY . .
RUN env CGOENABLED=0 GOOS=linux GOARCH=amd64 make install DESTDIR=/go/bin VERSION=${VERSION}

FROM gcr.io/distroless/base AS final
COPY --from=build /go/bin/assignment /
EXPOSE 8080/tcp
ENTRYPOINT ["/assignment", "-listen=:8080"]
CMD [""]
