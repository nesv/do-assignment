package proto

import (
	"net"
	"testing"
)

func TestClient(t *testing.T) {
	// Set up a server to use the client with.
	lst, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal("create listener:", err)
	}
	defer lst.Close()

}
