package proto

import (
	"fmt"
	"log"

	"assignment/pkg/store"
)

// ServerOption is a functional type that can be passed to NewServer
// to configure a Server before it is returned to the caller.
type ServerOption func(*Server) error

// WithStore sets the data store a Server will use to store the package
// index.
//
// By default, NewServer creates a server that will use and in-memory
// Store implementation.
// See the documentation for NewMemoryStore for more details.
func WithStore(s store.Store) ServerOption {
	return func(srv *Server) error {
		if srv.store != nil {
			if err := srv.store.Close(); err != nil {
				return fmt.Errorf("close previous datastore: %w", err)
			}
		}
		srv.store = s
		return nil
	}
}

// WithLogger allows the caller to provide a custom log.Logger when creating a
// server.
//
// By default, NewServer will instantiate a new server that logs to
// ioutil.Discard.
func WithLogger(logger *log.Logger) ServerOption {
	return func(srv *Server) error {
		srv.log = logger
		return nil
	}
}
