package strutil

import "testing"

func TestSliceIndex(t *testing.T) {
	tests := []struct {
		strs  []string
		input string
		want  int
	}{
		{
			strs:  []string{"a", "b", "c"},
			input: "b",
			want:  1,
		},
		{
			strs:  nil,
			input: "",
			want:  -1,
		},
		{
			strs:  []string{"foo", "bar", "baz"},
			input: "quux",
			want:  -1,
		},
	}

	for _, tt := range tests {
		if want, got := tt.want, SliceIndex(tt.strs, tt.input); want != got {
			t.Errorf("want=%d got=%d", want, got)
		}
	}
}
